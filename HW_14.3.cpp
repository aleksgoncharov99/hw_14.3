#include <iostream>

int main()
{
    std::string title("Example string");

    std::cout << title << "\n \n" << "Length: " << title.length() << "\n";
    std::cout << "First: " << title[0] << "\n" << "Last: " << title[title.length() - 1];
    
}